﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(BookPro))]
public class AutoFlip : MonoBehaviour
{
    public BookPro ControledBook;
    public FlipMode Mode;
    public float PageFlipTime = 1;
    public float DelayBeforeStart;
    public float TimeBetweenPages=5;
    public bool AutoStartFlip=true;
    bool flippingStarted = false;
    bool isPageFlipping = false;
    float elapsedTime = 0;
    float nextPageCountDown = 0;

    [SerializeField]
    private SoundManager _soundManager;

    // Use this for initialization
    void Start () {
        if (!ControledBook)
            ControledBook = GetComponent<BookPro>();
       // ControledBook.interactable = false;
        ControledBook.interactable = true;
        if (AutoStartFlip)
            StartFlipping();

        _soundManager = GetComponent<SoundManager>();
    }
    public void FlipRightPage()
    {
        if (isPageFlipping || ControledBook.GetPageDragging) return;

        if (ControledBook.CurrentPaper >= ControledBook.papers.Length-1) return; //forbid to flip last page
        isPageFlipping = true;
       
        PageFlipper.FlipPage(ControledBook, PageFlipTime, FlipMode.RightToLeft, ()=> { isPageFlipping = false; });
        PinchZoom.Instance.OnOneFingerMove.AddListener(MoveController.Instance.Move);    //Add Listener

        _soundManager.PlayNextAudio();

    }

    public void FlipLeftPage()
    {
        if (isPageFlipping || ControledBook.GetPageDragging) return;

        if (ControledBook.CurrentPaper <= 0) return;
        isPageFlipping = true;
        PageFlipper.FlipPage(ControledBook, PageFlipTime, FlipMode.LeftToRight, () => { isPageFlipping = false; });
        PinchZoom.Instance.OnOneFingerMove.AddListener(MoveController.Instance.Move);    //Add Listener

        _soundManager.PlayPreviousAudio();
    }
        
    public void StartFlipping()
    {
        flippingStarted = true;
        elapsedTime = 0;
        nextPageCountDown = 0;
    }
    void Update()
    {
        if (flippingStarted)
        {
            elapsedTime += Time.deltaTime;
            if (elapsedTime > DelayBeforeStart)
            {
                if (nextPageCountDown < 0)
                {
                    if ((ControledBook.CurrentPaper <= ControledBook.EndFlippingPaper &&
                        Mode == FlipMode.RightToLeft) ||
                        (ControledBook.CurrentPaper > ControledBook.StartFlippingPaper &&
                        Mode == FlipMode.LeftToRight))
                    {
                        isPageFlipping = true;
                        PageFlipper.FlipPage(ControledBook, PageFlipTime, Mode, ()=> { isPageFlipping = false; });
                    }
                    else
                    {
                        flippingStarted = false;
                        this.enabled = false;
                    }

                    nextPageCountDown = PageFlipTime + TimeBetweenPages+ Time.deltaTime;
                }
                nextPageCountDown -= Time.deltaTime;
            }
        }
    }
}
