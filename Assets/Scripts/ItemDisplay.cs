﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDisplay : MonoBehaviour {

    [SerializeField]
    private BookItem _itemPrefab;
    [SerializeField]
    private PinchZoom _zoom;

	public void ShowItems()
    {
        int counter = 0;
        foreach( Transform child in transform)
        {
            Destroy(child.gameObject);
        }

        foreach(ItemEntry item in BookShopManager.Instance.GetItemDataBase.itemList)
        {
            BookItem newBook = Instantiate(_itemPrefab) as BookItem;
            newBook.transform.SetParent(transform, false);

            newBook.GetOpenButton.onClick.AddListener(() => LoadAssetBundle.Instance.InstantiateObjectFromBundle(item.Name));
           // newBook.GetOpenButton.onClick.AddListener(() => _zoom.CloseMenu());

            newBook.Display(item);

            counter++;
        }
    }


   
}
