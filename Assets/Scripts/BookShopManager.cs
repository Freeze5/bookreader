﻿
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Collections;
using UnityEngine.Networking;


public class BookShopManager : MonoBehaviour
{

    public static BookShopManager Instance;

    private ItemDisplay _itemDisplay;


    [SerializeField]
    private ItemDataBase _itemDataBase;
    public ItemDataBase GetItemDataBase { get { return _itemDataBase; } }


    #region Previev asset bundle fields
    [Space]
    [SerializeField]
    private string _bundleUrl;

    [SerializeField]
    private string _previevBundleName;

    AssetBundle _myLoadedAssetbundle;

    [Space]
    [SerializeField]
    Sprite[] _loadedSprites;

    [Space]
    [SerializeField]
    private BookNames _bookNames;
    #endregion

    #region Json fields
    private string jsonPath;
    private string jsonName = "myJSON.json";
    #endregion


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }


    private void Start()
    {
        _itemDisplay = FindObjectOfType<ItemDisplay>();

        ReadJsonFile(jsonName);  // ReadJsonFile();
                                                                                
#if UNITY_ANDROID 
        _bundleUrl = Path.Combine(Application.streamingAssetsPath +"/AndroidBundles" , _previevBundleName );  // load bundle from local storage
        LoadPrevievBundle(_bundleUrl);                                          // load bundle from local storage

#endif


#if UNITY_IOS
        _bundleUrl = Path.Combine(Application.streamingAssetsPath + "/iOSBundles", _previevBundleName);
         LoadPrevievBundle(_bundleUrl);
#endif
    }

    #region Previev asset bundle methods

    /// <summary>
    /// Load asset bundle from local storage 
    /// </summary>
    /// <param name="Url"></param>
    private void LoadPrevievBundle(string Url)
    {
        _myLoadedAssetbundle = AssetBundle.LoadFromFile(Url);

        if (_myLoadedAssetbundle != null)
        {
            Debug.Log(" Previev assetbundle loaded successfully");
            _loadedSprites = _myLoadedAssetbundle.LoadAllAssets<Sprite>();
            Debug.Log(" Succesfully received " + _loadedSprites.Length + " sprites from previev assetbundle");

            AddBooksToTheItemList();
        }
        else
        {
            Debug.Log("Failed to load previev assetbundle");
            return;
        }
        
    }


    /// <summary>
    /// Load asset bundle from server
    /// </summary>
    /// <returns></returns>
    IEnumerator LoadPrevievFromServer()
    {
        var webRequest = UnityWebRequestAssetBundle.GetAssetBundle(_bundleUrl);
        yield return webRequest.SendWebRequest();

        if (webRequest.isNetworkError || webRequest.isHttpError)
        {
            Debug.Log("Failed to get previev webRequest! Please check your internet access");
            yield return null;
        }
        else
        {
            Debug.Log("Previev webRequest successfully recieved from server ");
            _myLoadedAssetbundle = DownloadHandlerAssetBundle.GetContent(webRequest);
            if (_myLoadedAssetbundle != null)
            {
                Debug.Log("Previev assetbundle successfully loaded from server ");
   
                _loadedSprites = _myLoadedAssetbundle.LoadAllAssets<Sprite>();
                Debug.Log(" Succesfully received " + _loadedSprites.Length + " sprites from previev assetbundle");

                AddBooksToTheItemList();
            }
            else
            {
                Debug.Log("Failed to load previev assetbundle from server");
            }
        }
        
    }

    /// <summary>
    /// Add loaded previev sprites and bookNames to the each item of itemlist
    /// </summary>
    void AddBooksToTheItemList()
    {

        int counter = 0;
        foreach (var sprite in _loadedSprites)
        {
             var item = new ItemEntry(sprite, _bookNames._loadedNames[counter]);
            // var item = new ItemEntry(sprite, "Test");
            _itemDataBase.itemList.Add(item);
            counter++;
        }
        _itemDisplay.ShowItems();
    }

#endregion



    void ReadJsonFile( string fileName)
    {
        jsonPath = Path.Combine(Application.streamingAssetsPath + "/" , fileName);
        string jsonString;
#if UNITY_EDITOR || UNITY_IOS
        jsonString = File.ReadAllText(jsonPath);
            Debug.Log(jsonString + "--> Editor & iOS");

            _bookNames = JsonUtility.FromJson<BookNames>(jsonString); 
#endif

#if UNITY_ANDROID
        WWW reader = new WWW (jsonPath);
        while (!reader.isDone) {
         }
        jsonString = reader.text;
        Debug.Log(jsonString + "--> Android");

        _bookNames = JsonUtility.FromJson<BookNames>(jsonString);
#endif
    }
  
}

[System.Serializable]
public class ItemDataBase
{
    public List<ItemEntry> itemList = new List<ItemEntry>() ; 
}


[System.Serializable]
public class ItemEntry
{
    public Sprite Previev;
    public string Name;

    public ItemEntry(Sprite previev, string name)
    {
        Previev = previev;
        Name = name;
    }
}


[System.Serializable]
public class BookNames
{
    public List<string> _loadedNames;

    public BookNames ( List<string> names)
    {
        _loadedNames = names;
    }
}
