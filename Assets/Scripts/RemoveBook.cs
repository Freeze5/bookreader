﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveBook : MonoBehaviour {


    public void RemoveTheBook()
    {
       // PinchZoom.Instance.isInMenu = true;
        PinchZoom.Instance.OnOneFingerMove.RemoveListener(MoveController.Instance.Move);    //remove Listener

        Destroy(this.gameObject);
    }
}
