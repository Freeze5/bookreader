﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//should be attached on canvas
public class PinchZoom : MonoBehaviour {

    public static PinchZoom Instance;
   // public Canvas _canvas;

    [SerializeField]
    private GameObject _thebook;

    [Space]
    [Header("Zoom")]
    [SerializeField]
    private float _zoomSpeed;


    /// <summary>
    /// Минимальная дистация увеличения
    /// </summary>
    /// 
    [SerializeField]
    private float _minZoomX , _minZoomY;
    /// <summary>
    /// Максиммальная дистация увеличения
    /// </summary>
    [SerializeField]
    private float _maxZoom;

   // public bool isInMenu;

    public bool isZoomed = false;

    /// <summary>
    /// Когда 1 палец в движении
    /// </summary>
    public UnityOneFingerTouchEvent OnOneFingerMove = new UnityOneFingerTouchEvent();

   

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;

        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        //isInMenu = true;

        //_canvas = FindObjectOfType<Canvas>();
        _minZoomX = _thebook.transform.localScale.x;
        _minZoomY = _thebook.transform.localScale.y;
    }

    // Update is called once per frame
    void Update ()
    {
        
            Zooming();
        

        Inputs();
    }

    public void Zooming()
    {
        if(Input.touchCount == 2 )
        {
            Touch touch0 = Input.GetTouch(0);
            Touch touch1 = Input.GetTouch(1);

            //Позиция в предыдущем кадре каждого касания.
            Vector2 touchZeroPrevPos = touch0.position - touch0.deltaPosition;
            Vector2 touchOnePrevPos = touch1.position - touch1.deltaPosition;

            //Величина вектора между касаниями
            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touch0.position - touch1.position).magnitude;

            //Найти разницу между касаниями.
            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            Vector3 bookTransform = _thebook.transform.localScale;

            bookTransform.x -= deltaMagnitudeDiff * _zoomSpeed * 100;
            bookTransform.y -= deltaMagnitudeDiff * _zoomSpeed * 100;


            bookTransform.x = Mathf.Clamp(bookTransform.x + deltaMagnitudeDiff * _zoomSpeed, _minZoomX, _maxZoom);
            bookTransform.y = Mathf.Clamp(bookTransform.y + deltaMagnitudeDiff * _zoomSpeed, _minZoomY, _maxZoom);

            _thebook.transform.localScale = bookTransform;

            // _canvas.scaleFactor -= deltaMagnitudeDiff * _zoomSpeed *100;

            //_canvas.scaleFactor = Mathf.Clamp(_canvas.scaleFactor + deltaMagnitudeDiff * _zoomSpeed, _minZoom, _maxZoom);

            // float currentScale = _canvas.scaleFactor;

            float currentScaleX = _thebook.transform.localScale.x;
            if(currentScaleX > _minZoomX)
            {
                isZoomed = true;
            }
            else
            {
                isZoomed = false;
                MoveController.Instance.gameObject.transform.position = MoveController.Instance.GetStartPos;
            }
            
        }
       
    }

    void Inputs()
    {

        if (Input.touchCount == 1)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                OnOneFingerMove.Invoke(Input.GetTouch(0));
            }
        }
        
    }


    public void CloseMenu()
    {
        //isInMenu = false;
    }
 
}

public class UnityOneFingerTouchEvent : UnityEvent<Touch>
{

}
