﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//should be attached at the book with the AudioSource componenet
public class SoundManager : MonoBehaviour
{
    [SerializeField]
    private AudioSource _audioSource; 

    [SerializeField]
    private Button _muteButton;      //darag and drop btn from scene

    public AudioClip[] _audioClips;  //darag and drop _audioClips from project

    [SerializeField]
    private Sprite[] _sprite;

    [SerializeField]
    int _audioIndex;


    public bool isMuted =false;
    

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        _audioIndex = 0;

        _audioSource.clip = _audioClips[_audioIndex];
        if ( !_audioSource.mute)
        {
            _audioSource.Play();
        }

        _muteButton.onClick.AddListener(ExecuteMute);
        _muteButton.image.sprite = _sprite[0];

    }

   public void PlayNextAudio()
    {
        if(_audioIndex < _audioClips.Length -1)
        {
            _audioIndex++;
        }
        else
        {
            Debug.Log("You have riched page's limit");
        }
      
        for (int i= _audioIndex; i < _audioClips.Length; i++)
        {
            if(_audioIndex -1 <= _audioClips.Length)
            {
                GetAudioClip(_audioClips[_audioIndex]);
               
            }
            else
            {
                Debug.Log("Audio clips finished !");
            }
        }
        
    }

    public void PlayPreviousAudio()
    {

        if (_audioIndex > 0)
        {
            _audioIndex--;
        }
        else
        {
            Debug.Log("You have riched page's limit");
        }

        for (int i = _audioIndex; i < _audioClips.Length; i++)
        {
            if (_audioIndex >=0)
            {
                GetAudioClip(_audioClips[_audioIndex]);
               
            }
            else
            {
                Debug.Log("Audio clips finished !");
            }
        }

    }

    private void GetAudioClip( AudioClip clip)
    {
        if ( isMuted)
        {
            _audioSource.clip = clip;
            _audioSource.Stop();
        }
        else
        {
            _audioSource.clip = clip;
            _audioSource.Play();
        }

       
    }


    private void ExecuteMute()
    {
        if (!isMuted)
        {
            //_audioSource.mute = true;
            _audioSource.Pause();
            isMuted = true;
            _muteButton.image.sprite = _sprite[1];

            Debug.Log("Mute sound");
        }
        else if( isMuted)
        {
            // _audioSource.mute = false;
            _audioSource.Play();
            isMuted = false;
            _muteButton.image.sprite = _sprite[0];

            Debug.Log("UnMute sound");
        }
    }

    
}
