﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Networking;

/// <summary>
/// Allows you to load asset bundle with full books
/// </summary>
public class LoadAssetBundle : MonoBehaviour {

    public static LoadAssetBundle Instance;

    [SerializeField]
    private Canvas _canvas;

    AssetBundle _myLoadedAssetbundle;

    public string _bundleNameAndroid;

    public string _bundleNameiOS;

    [SerializeField]
    private string _bundleUrl;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    // Use this for initialization
    void Start () {
        _canvas = GetComponent<Canvas>();

#if UNITY_ANDROID 
        _bundleUrl = Path.Combine(Application.streamingAssetsPath + "/AndroidBundles", _bundleNameAndroid ); // load bundle from local storage
        LoadAssetBundleObject(_bundleUrl);                                              // load bundle from local storage

#endif


#if UNITY_IOS
        _bundleUrl = Path.Combine(Application.streamingAssetsPath + "/iOSBundles", _bundleNameiOS);
        LoadAssetBundleObject(_bundleUrl);                                                     // load bundle from local storage
#endif
    }

    /// <summary>
    /// Load asset bundle from local storage
    /// </summary>
    /// <param name="bundleUrl"></param>
    void LoadAssetBundleObject( string bundleUrl)
    {
        _myLoadedAssetbundle = AssetBundle.LoadFromFile(bundleUrl);

        if(_myLoadedAssetbundle == null)
        {
            Debug.Log("Failed to load books assetbundle");
        }
        else
        {
            Debug.Log("Books Assetbundle loaded successfully");

        }
    }


    /// <summary>
    /// Load asset bundle from server
    /// </summary>
    /// <returns></returns>
    IEnumerator LoadBookBundleFromServer()
    {
        var webRequest = UnityWebRequestAssetBundle.GetAssetBundle(_bundleUrl);
        yield return webRequest.SendWebRequest();

        if (webRequest.isNetworkError || webRequest.isHttpError)
        {
            Debug.Log("Failed to get webRequest! Please check your internet access");
            yield return null;
        }
        else
        {
            Debug.Log("webRequest successfully recieved from server ");
            _myLoadedAssetbundle = DownloadHandlerAssetBundle.GetContent(webRequest);
            if(_myLoadedAssetbundle == null)
               {
                Debug.Log("Failed to load books assetbundle from server");
                }
               else
               {
                Debug.Log("Books Assetbundle successfully loaded from server");
            }
        }
    }

    /// <summary>
    /// Initialize asset bundle content
    /// </summary>
    /// <param name="assetName"></param>
    public void InstantiateObjectFromBundle(string assetName)
    {
        var prefab = _myLoadedAssetbundle.LoadAsset(assetName) as GameObject;
        
        Instantiate(prefab,_canvas.transform);
    }

  
}
