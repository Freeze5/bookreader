﻿using UnityEngine;
using UnityEngine.Events;

public class InputManager : MonoBehaviour
{
    /// <summary>
    /// Когда 1 палец прикоснулся к экрану
    /// </summary>
    public static UnityOneTouchEvent OnOneFingerStart = new UnityOneTouchEvent();
    /// <summary>
    /// Когда 1 палец в движении
    /// </summary>
    public static UnityOneTouchEvent OnOneFingerMove = new UnityOneTouchEvent();
    /// <summary>
    /// Когда 1 палец убран с экрана либо пользователь добавил второй
    /// </summary>
    public static UnityEvent OnOneFingerEnd = new UnityEvent();
    /// <summary>
    /// Когда к екрану прикоснулось 2 пальца
    /// </summary>
    public static UnityTwoTouchEvent OnTwoFingerStart = new UnityTwoTouchEvent();
    /// <summary>
    /// Когда 1 из двух пальцев в движении
    /// </summary>
    public static UnityTwoTouchEvent OnTwoFingerMove = new UnityTwoTouchEvent();
    /// <summary>
    /// Когда меньше двух пальцев
    /// </summary>
    public static UnityEvent OnTwoFingerEnd = new UnityEvent();
    /// <summary>
    /// Когда крутится колесико мышки
    /// </summary>
    public static UnityMouseScrollEvent OnMouseScroll = new UnityMouseScrollEvent();

    private Touch _lastFakeTouch = new Touch();


    private void Update()
    {
        AndroidInputs();
#if UNITY_EDITOR
        EditorInputs();
#endif
    }

    private void AndroidInputs()
    {
        if (Input.touchCount == 1)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                OnOneFingerStart.Invoke(Input.GetTouch(0));
            }

            if (Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                OnOneFingerMove.Invoke(Input.GetTouch(0));
            }

            if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                OnOneFingerEnd.Invoke();
            }
        }

        if (Input.touchCount == 2)
        {
            OnOneFingerEnd.Invoke();

            if (Input.GetTouch(0).phase == TouchPhase.Began || Input.GetTouch(1).phase == TouchPhase.Began)
            {
                OnTwoFingerStart.Invoke(Input.GetTouch(0), Input.GetTouch(1));
            }

            if (Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(1).phase == TouchPhase.Moved)
            {
                OnTwoFingerMove.Invoke(Input.GetTouch(0), Input.GetTouch(1));
            }
        }
        else
        {
            OnTwoFingerEnd.Invoke();
        }
    }

    private void EditorInputs()
    {
        float scrollWheelAxis = Input.GetAxis("Mouse ScrollWheel");
        
        if (scrollWheelAxis != 0f) // forward
        {
            OnMouseScroll.Invoke(scrollWheelAxis);
        }

        FakeTouchInput faketouch = FakeTouch();

        if (faketouch == null)
            return;

        if (faketouch.touch.phase == TouchPhase.Began)
        {
            OnOneFingerStart.Invoke(faketouch.touch);
        }

        if (faketouch.touch.phase == TouchPhase.Moved)
        {
            OnOneFingerMove.Invoke(faketouch.touch);
        }

        if (faketouch.touch.phase == TouchPhase.Ended)
        {
            OnOneFingerEnd.Invoke();
        }
    }

    private FakeTouchInput FakeTouch()
    {
        FakeTouchInput faketouch = new FakeTouchInput();
        Touch touch = new Touch();
        if (Input.GetMouseButtonDown(0))
        {
            touch.phase = TouchPhase.Began;
            touch.deltaPosition = new Vector2(0, 0);
            touch.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            touch.fingerId = 0;

            faketouch.touch = touch;
            _lastFakeTouch = touch;

            return faketouch;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            touch.phase = TouchPhase.Ended;
            Vector2 newPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            touch.deltaPosition = newPosition - _lastFakeTouch.position;
            touch.position = newPosition;
            touch.fingerId = 0;

            faketouch.touch = touch;
            _lastFakeTouch = touch;

            return faketouch;
        }
        else if (Input.GetMouseButton(0))
        {
            touch.phase = TouchPhase.Moved;
            Vector2 newPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            touch.deltaPosition = newPosition - _lastFakeTouch.position;
            touch.position = newPosition;
            touch.fingerId = 0;

            faketouch.touch = touch;
            _lastFakeTouch = touch;

            return faketouch;
        }
        else
        {
            return faketouch = null;
        }
    }
}

public class UnityOneTouchEvent : UnityEvent<Touch>
{

}
public class UnityTwoTouchEvent : UnityEvent<Touch, Touch>
{

}

public class UnityMouseScrollEvent : UnityEvent<float>
{

}

public class FakeTouchInput
{
    public Touch touch;
}