﻿using UnityEngine;

public class BookGestureControll : MonoBehaviour
{
    [SerializeField]
    private Camera _camera;
    /// <summary>
    /// Скорость перемещения камеры
    /// </summary>
    [Space]
    [Header("Drag")]
    [SerializeField]
    private float _dragSpeed;
    /// <summary>
    /// Скорость зума
    /// </summary>
    [Space]
    [Header("Zoom")]
    [SerializeField]
    private float _zoomSpeed;
    /// <summary>
    /// Минимальная дистация увеличения
    /// </summary>
    [SerializeField]
    private float _minZoom;
    /// <summary>
    /// Максиммальная дистация увеличения
    /// </summary>
    [SerializeField]
    private float _maxZoom;
    
    private bool _isMove;

    [SerializeField]
    Canvas _canvas;

    private void Awake()
    {
        Screen.orientation = ScreenOrientation.Portrait;
    }

    private void OnEnable()
    {
        InputManager.OnOneFingerMove.AddListener(Move);
        InputManager.OnTwoFingerMove.AddListener(Zoom);
        InputManager.OnMouseScroll.AddListener(Zoom); 
    }

    private void OnDisable()
    {        
        InputManager.OnOneFingerMove.RemoveListener(Move);
        InputManager.OnTwoFingerMove.RemoveListener(Zoom);
        InputManager.OnMouseScroll.RemoveListener(Zoom);
    }
    
    #region Move

    private void Move(Touch touch)
    {
        transform.position = new Vector3(transform.position.x - touch.deltaPosition.x * _dragSpeed,
            transform.position.y - touch.deltaPosition.y * _dragSpeed, transform.position.z);
    }
    
    #endregion

    #region Zoom

    private void Zoom(Touch touch0, Touch touch1)
    {
        var deltaMagnitudeDiff = Difference(touch0, touch1);
        
        //Изменить орфографический размер, основанный на изменении расстояния между касаниями
        //_camera.orthographicSize += deltaMagnitudeDiff * _zoomSpeed;
       _canvas.scaleFactor += deltaMagnitudeDiff * _zoomSpeed;


        //_camera.orthographicSize = Mathf.Clamp(_camera.orthographicSize, _minZoom, _maxZoom);
       _canvas.scaleFactor = Mathf.Clamp(_canvas.scaleFactor, _minZoom, _maxZoom);

    }

    private void Zoom(float delta)
    {
        //100 - коэффициент для мобилок
        // _camera.orthographicSize += delta * _zoomSpeed * 100f;
        _canvas.scaleFactor += delta * _zoomSpeed * 100;


        //_camera.orthographicSize = Mathf.Clamp(_camera.orthographicSize + delta * _zoomSpeed, _minZoom, _maxZoom);

        _canvas.scaleFactor = Mathf.Clamp(_canvas.scaleFactor + delta * _zoomSpeed, _minZoom, _maxZoom);

    }

    /// <summary>
    /// Найти разницу дистанции между касаниями
    /// </summary>
    /// <param name="touch0">Первое касание</param>
    /// <param name="touch1">Второе касание</param>
    /// <returns></returns>
    private float Difference(Touch touch0, Touch touch1)
    {
        //Позиция в предыдущем кадре каждого касания.
        Vector2 touchZeroPrevPos = touch0.position - touch0.deltaPosition;
        Vector2 touchOnePrevPos = touch1.position - touch1.deltaPosition;

        //Величина вектора между касаниями
        float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
        float touchDeltaMag = (touch0.position - touch1.position).magnitude;

        //Найти разницу между касаниями.
        float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

        return deltaMagnitudeDiff;
    }
    
    #endregion
}