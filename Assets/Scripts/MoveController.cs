﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveController : MonoBehaviour {

    public static MoveController Instance;

    [Space]
    [Header("Drag")]
    [SerializeField]
    private float _dragSpeed;

    [SerializeField]
    Vector3 startPos;

    public Vector3 GetStartPos { get { return startPos; } }

    [SerializeField]
    private float _dragAmplitudeX;
    [SerializeField]
    private float _dragAmplitudeY;



    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

   

    private void Start()
    {
        startPos = transform.position;
    }

    public void Move(Touch touch)
    {
        if (PinchZoom.Instance.isZoomed)
        {
           // transform.position = new Vector3(transform.position.x - touch.deltaPosition.x * -_dragSpeed,transform.position.y - touch.deltaPosition.y * -_dragSpeed, transform.position.z);
            transform.position = new Vector3(Mathf.Clamp (transform.position.x - touch.deltaPosition.x * -_dragSpeed,startPos.x - _dragAmplitudeX, startPos.x + _dragAmplitudeX),
            Mathf.Clamp( transform.position.y - touch.deltaPosition.y * -_dragSpeed,startPos.y - _dragAmplitudeY, startPos.y + _dragAmplitudeY), transform.position.z);
        }
        else
        {
            transform.position = startPos;
        }
        
    }

}
