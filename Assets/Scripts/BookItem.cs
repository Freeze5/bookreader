﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BookItem : MonoBehaviour {
    
    [SerializeField]
    private Text _bookName;
    [SerializeField]
    private Image _bookPreviev;

    [SerializeField]
    private Button _openButton;
    public Button GetOpenButton { get { return _openButton; } }



    public void Display(ItemEntry item)
    {
        _bookName.text = item.Name;
        _bookPreviev.sprite = item.Previev;
    }
}
